import re
from typing import Tuple, List
from urllib.request import urlopen

#from db import Cass_db


class Country_list:
    stored: int
    data: List[Tuple[str, str, str, str]]

    def __init__(self, db):
        self.db = db
        self.data = self.__load()
        self.stored = len(self.data)

    def __load(self) -> List[Tuple[str, str, str, str]]:
        ret = []
        table = self.db.select_all_countries()
        if not len(table):
            return ret

        for row in table:
            ret.append(tuple(row))
        return ret

    def add(self, country: Tuple[str, str, str, str]) -> None:
        self.data.append(country)

    def store(self) -> None:
        self.db.store_countries_table(self.data[self.stored: len(self.data)])
        self.stored = len(self.data)
        pass

    # list of tuples of 4 values (code, name, currency, coefficient)
    def get(self, code: str, coef: str = '') -> Tuple[str, str, str, str]:
        if len(self.data):
            for x in self.data:
                if x[0] == code:
                    return x
        return code, '', '', coef

    def scrape(self, date: str, code: str = None) -> bool:
        # already have country
        if code is not None:
            pom = self.get(code)
            if not self.get(code)[1] == '':
                return True

        # don't have
        ret = False
        url = f"https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu" \
              f"/denni_kurz.txt;?date={date} "
        page = urlopen(url)
        html_bytes = page.read()
        html = html_bytes.decode("utf-8")
        # ignore first two lines (until 'kurz') ten separate lines by '\n'
        for country in filter(None, (re.split(f'.*\n.*kurz\n', html)[1:])[0].split('\n')):
            pom = country.split("|")
            if self.get(pom[3])[1] == '':
                self.add((pom[3], pom[2], pom[0], pom[1]))
                if pom[3] == code:
                    ret = True
        return ret
