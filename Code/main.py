from datetime import datetime
from typing import Tuple, List

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap, QPainter
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtChart import QChart, QChartView, QValueAxis, QBarCategoryAxis, QBarSet, QBarSeries, QPercentBarSeries

import sys
import mysql.connector
from PyQt5.QtWidgets import QTableWidget

import pandas as pd
import scipy.cluster.hierarchy as hac
from scipy.cluster.hierarchy import fcluster
import matplotlib.pyplot as plt
from collections import defaultdict

from numpy import sqrt, mean, log, diff
import quandl

from db import Cass_db


class Database:
    def __init__(self):
        self.mySQLdb = mysql.connector.connect(
            host="91.223.69.6",
            user="raitranssk_upab",
            password="HdDbB8X*//",
            database="raitranssk_upab",
            connect_timeout=10
        )

        self.mySQLcursor = self.mySQLdb.cursor()
        self.no_sql_db = Cass_db()
        keyspace = "foreign_exchange"
        self.no_sql_db.prepare(keyspace)
        self.chceck_sql_database_state()

    def chceck_sql_database_state(self):
        self.mySQLcursor.execute("SHOW TABLES LIKE 'Mena';")
        res = self.mySQLcursor.fetchall()
        if not res:
            self.drop_all_tables()
            self.create_tables()
            self.init_tables(self.get_all_nosql_data())

        count = self.get_record_count()
        if count == 0:
            self.drop_all_tables()
            self.create_tables()
            self.init_tables(self.get_all_nosql_data())

    def drop_all_tables(self):
        self.mySQLcursor.execute("SET FOREIGN_KEY_CHECKS = 0")
        self.mySQLcursor.execute("DROP TABLE IF EXISTS Nazov_meny")
        self.mySQLcursor.execute("DROP TABLE IF EXISTS Mena ")

    def create_tables(self):

        self.mySQLcursor.execute("CREATE TABLE Nazov_meny ("
                                 "skratka VARCHAR(4) NOT NULL,"
                                 "nazov VARCHAR(50) NOT NULL,"
                                 "zem VARCHAR(25) NOT NULL,"
                                 "mnozstvo INT NOT NULL,"
                                 "PRIMARY KEY ( skratka )"
                                 ")")

        self.mySQLcursor.execute("CREATE TABLE Mena ("
                                 "skratka VARCHAR(4) NOT NULL ,"
                                 "kurz FLOAT NOT NULL ,"
                                 "datum DATE NOT NULL,"
                                 "CONSTRAINT PK_Mena PRIMARY KEY (skratka,datum),"
                                 "FOREIGN KEY (skratka) REFERENCES Nazov_meny(skratka)"
                                 ")")

        self.mySQLcursor.execute("ALTER TABLE Mena ADD INDEX datum_index (datum)")

    def init_tables(self, data_from_nosql):
        set_of_currency = set()
        i = 0
        for key in data_from_nosql:
            print("zapisujem deň: %s " % key)
            for currency in data_from_nosql[key]:
                set_of_currency.add((currency[0], currency[1], currency[2], currency[3]))
                self.mySQLcursor.execute("INSERT INTO Mena(skratka, kurz, datum) "
                                         "VALUES ('%s', '%s', '%s')" % (currency[0], currency[4], key))
                if i % 100 == 0: self.mySQLdb.commit()
                i += 1

        for currency in set_of_currency:
            self.mySQLcursor.execute("INSERT IGNORE INTO Nazov_meny(skratka, nazov, zem, mnozstvo) "
                                     "VALUES ('%s', '%s', '%s', '%s')"
                                     % (currency[0], currency[1], currency[2], currency[3]))
        self.mySQLdb.commit()

    def get_actual_data(self):
        result = []
        self.mySQLcursor.execute("SELECT zem, nazov, mnozstvo, Nazov_meny.skratka, kurz, datum "
                                 "FROM "
                                 "(SELECT * FROM Mena "
                                 "WHERE "
                                 "datum = (SELECT "
                                 "MAX(datum) "
                                 "FROM Mena) ) T"
                                 "INNER JOIN Nazov_meny ON TINNER.skratka = Nazov_meny.skratka"
                                 )

        my_result = self.mySQLcursor.fetchall()

        result.append('%s. %s. %s' % (my_result[0][5].day, my_result[0][5].month, my_result[0][5].year))

        for x in my_result:
            result.append((x[0], x[1], str(x[2]), x[3], str(x[4])))

        return result

    def get_data_by_date(self, date_from, date_to):
        result = []
        self.mySQLcursor.execute("SELECT zem, nazov, Nazov_meny.skratka, kurz "
                                 "FROM "
                                 "(SELECT * FROM Mena "
                                 "WHERE "
                                 "datum = '%s' ) T"
                                 "INNER JOIN Nazov_meny ON TINNER.skratka = Nazov_meny.skratka"
                                 % date_from)

        begin = self.mySQLcursor.fetchall()

        for x in begin:
            result.append((x[0], x[1], str(x[2]), x[3]))

        self.mySQLcursor.execute("SELECT zem, nazov, Nazov_meny.skratka, kurz "
                                 "FROM "
                                 "(SELECT * FROM Mena "
                                 "WHERE "
                                 "datum = '%s' ) T"
                                 "INNER JOIN Nazov_meny ON TINNER.skratka = Nazov_meny.skratka"
                                 % date_to)

        end = self.mySQLcursor.fetchall()

        for x in end:
            result.append((x[0], x[1], str(x[2]), x[3]))

        result = [begin] + [end]
        return result

    def get_data_from_to_date(self, date_from, date_to):
        result = []
        self.mySQLcursor.execute("SELECT skratka, kurz, datum "
                                 "FROM "
                                 "Mena "
                                 "WHERE "
                                 "datum >= '%s' AND datum <= '%s' "
                                 % (date_from, date_to))

        begin = self.mySQLcursor.fetchall()

        for x in begin:
            result.append((x[0], x[1], x[2]))
        return result

    def get_record_count(self):
        self.mySQLcursor.execute("SELECT COUNT(*) FROM Mena")
        begin = self.mySQLcursor.fetchall()
        return begin[0][0]

    def get_last_date(self):
        self.mySQLcursor.execute("SELECT MAX(datum) "
                                 "FROM Mena ")
        res = self.mySQLcursor.fetchall()[0][0]
        return res.strftime("%Y-%m-%d")

    def get_first_date(self):
        self.mySQLcursor.execute("SELECT MIN(datum) "
                                 "FROM Mena ")
        res = self.mySQLcursor.fetchall()[0][0]
        return res.strftime("%Y-%m-%d")

    def get_set_of_currencies(self):
        self.mySQLcursor.execute("SELECT skratka, nazov, zem, mnozstvo "
                                 "FROM Nazov_meny ")
        return self.mySQLcursor.fetchall()

    def get_nosql_data(self, start: str, stop: str):
        ret = {}
        data = self.no_sql_db.select_daily_rates_by_date(start, stop)
        date = 0
        for x in data:
            if not date == x[1]:
                date = x[1]
                ret[f"{x[1]}"] = []
            ret[f"{x[1]}"].append((x[2], x[5], x[4], x[3], x[6]))
        return ret

    def get_all_nosql_data(self):
        if self.no_sql_db.should_init():
            self.no_sql_db.init_nosql_data()
        else:
            self.no_sql_db.update_nosql_data()
        now = datetime.now()
        return self.get_nosql_data('1991-01-01', f"{now.year:04}-{now.month:02}-{now.day:02}")

    def get_update_data_from_nosql(self):
        last_date_from_sql = self.get_last_date()
        now = datetime.now()
        if f"{now.year:04}-{now.month:02}-{now.day:02}" == last_date_from_sql:
            self.update_database(self.get_nosql_data(last_date_from_sql, f"{now.year:04}-{now.month:02}-{now.day:02}"))

    def update_database(self, data_from_nosql):
        if not data_from_nosql:
            pass
        else:
            set_of_currency_from_sql = set()
            set_of_currency_from_sql = self.get_set_of_currencies()
            set_of_currency_from_nosql = set()

            for key in data_from_nosql:
                for currency in data_from_nosql[key]:
                    set_of_currency_from_nosql.add((currency[0], currency[1], currency[2], currency[3]))
                    self.mySQLcursor.execute("INSERT INTO Mena(skratka, kurz, datum) "
                                             "VALUES ('%s', '%s', '%s')" % (currency[0], currency[4], key))

            if set_of_currency_from_sql != set_of_currency_from_nosql:
                set_of_currency_from_nosql = set_of_currency_from_nosql.difference(set_of_currency_from_sql)
                for currency in set_of_currency_from_nosql:
                    self.mySQLcursor.execute("INSERT INTO Nazov_meny(skratka, nazov, zem, mnozstvo) "
                                             "VALUES ('%s', '%s', '%s', '%s')"
                                             % (currency[0], currency[1], currency[2], currency[3]))
            self.mySQLdb.commit()

    def close(self):
        self.mySQLdb.cursor().close()
        self.mySQLdb.close()


class DendogramWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout()
        self.vert_layout = QHBoxLayout()
        self.layout.addLayout(self.vert_layout)
        self.label_img = QLabel(self)
        self.pixmap = QPixmap('img/dendogram.png')
        self.label_img.setPixmap(self.pixmap)
        self.label_img.setFixedSize(self.pixmap.width(), self.pixmap.height())
        self.vert_layout.addWidget(self.label_img)
        self.setLayout(self.layout)
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.accepted.connect(self.close_this_window)
        self.setStyleSheet('''
                            background-color:#ffffff;
                            ''')
        self.layout.addWidget(self.buttonBox)

    def close_this_window(self):
        self.close()


class AnotherWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("O projekte")
        self.layout = QVBoxLayout()
        self.vert_layout = QHBoxLayout()
        self.layout.addLayout(self.vert_layout)
        self.label_img = QLabel(self)
        self.label_img.setStyleSheet("margin-right:30px;margin-left:30px;")
        self.pixmap = QPixmap('img/FIT_zkracene_barevne_RGB_CZ.resized.png')
        self.label_img.setPixmap(self.pixmap)
        self.vert_layout.addWidget(self.label_img)
        self.label = QLabel("O projekte \n"
                            "Tento program vznikol ako riešenie projektu "
                            "do predmetu UPA. Cieľom projektu je zozbierať "
                            "informácie o kurzových lístkoch mien vzhľadom"
                            "ku Českej korune a graficky podať užívateľovi\n\n"
                            "Autory:\t Kristián Barna (xbarna02)\n"
                            "\t Martin Uhliar (xuhlia03)\n"
                            "Rok:\t 2020")
        self.label.setWordWrap(True)
        self.Width = 600
        self.height = 240
        self.resize(self.Width, self.height)
        self.setStyleSheet('''
                    background-color:#adacaa;
                    font-family: "Arial"; 
                    ''')
        self.vert_layout.addWidget(self.label)
        self.setLayout(self.layout)
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.accepted.connect(self.close_this_window)
        self.layout.addWidget(self.buttonBox)

    def close_this_window(self):
        self.close()


class HelpWindow(QWidget):
    def __init__(self, help_str):
        super().__init__()
        self.vert_layout = QVBoxLayout()
        self.label = QLabel(help_str)
        self.label.setWordWrap(True)
        self.Width = 500
        self.height = 180
        self.resize(self.Width, self.height)
        self.setStyleSheet('''
                    background-color:#adacaa;
                    font-family: "Arial"; 
                    ''')
        self.vert_layout.addWidget(self.label)
        self.setLayout(self.vert_layout)
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.accepted.connect(self.close_this_window)
        self.vert_layout.addWidget(self.buttonBox)

    def close_this_window(self):
        self.close()


class TableViewActual(QTableWidget):
    def __init__(self, arr_data, *args):
        QTableWidget.__init__(self, *args)
        self.data = arr_data
        self.set_data()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()

    def set_data(self):
        self.setHorizontalHeaderLabels(('Zem', 'Mena', 'Množstvo', 'Kód', 'Kurz'))
        i = 0
        for row in self.data:
            j = 0
            for cell in row:
                new_item = QTableWidgetItem(cell)
                self.setItem(i, j, new_item)
                j += 1
            i += 1


class TableViewQueryA(QTableWidget):
    def __init__(self, arr_data, *args):
        QTableWidget.__init__(self, *args)
        self.data = arr_data
        self.set_data()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()

    def set_data(self):
        self.setHorizontalHeaderLabels(('Kód', 'Krajina', 'Kurz pred', 'Kurz po', 'Zmena', '(%)'))
        i = 0
        for row in self.data:
            j = 0
            for cell in row:
                new_item = QTableWidgetItem(cell)
                self.setItem(i, j, new_item)
                j += 1
            i += 1


class TableViewQueryB(QTableWidget):
    def __init__(self, arr_data, *args):
        QTableWidget.__init__(self, *args)
        self.data = arr_data
        self.set_data()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()

    def set_data(self):
        self.setHorizontalHeaderLabels(('Trieda', 'Zahrnuté meny'))
        i = 0
        for row in self.data:
            j = 0
            for cell in row:
                new_item = QTableWidgetItem(cell)
                self.setItem(i, j, new_item)
                j += 1
            i += 1


class Dialog(QDialog):
    NumGridRows = 3
    NumButtons = 4

    def __init__(self):
        super(Dialog, self).__init__()
        self.create_form_group_box()
        self.form_group_box = QGroupBox("Form layout")

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.formGroupBox)
        main_layout.addWidget(button_box)
        self.setLayout(main_layout)

        self.setWindowTitle("Chyba")

    def create_form_group_box(self):
        layout = QFormLayout()
        layout.addRow(QLabel("Name:"), QLineEdit())
        layout.addRow(QLabel("Country:"), QComboBox())
        layout.addRow(QLabel("Age:"), QSpinBox())
        self.form_group_box.setLayout(layout)


class AlignDelegate(QItemDelegate):
    def paint(self, painter, option, index):
        option.displayAlignment = QtCore.Qt.AlignCenter
        QItemDelegate.paint(self, painter, option, index)


class Window(QMainWindow):
    def __init__(self):
        super().__init__()

        self.db = Database()

        self.actual_date = ''
        self.actual_data = []

        self.table_query_a = TableViewQueryA([], 0, 6)
        self.table_query_b = TableViewQueryB([], 0, 2)

        self.date_edit_from = QtWidgets.QDateEdit(calendarPopup=True)
        self.date_edit_to = QtWidgets.QDateEdit(calendarPopup=True)
        self.date_edit_from_b = QtWidgets.QDateEdit(calendarPopup=True)
        self.date_edit_to_b = QtWidgets.QDateEdit(calendarPopup=True)
        self.date_edit_from_custom = QtWidgets.QDateEdit(calendarPopup=True)
        self.date_edit_to_custom = QtWidgets.QDateEdit(calendarPopup=True)
        self.help_ui1_str = "Aktuálny prehľad kurzov\n\n" \
                            "Na tejto stránke sa zobrazujú informácie o aktuálnych kurzoch mien voči Českej " \
                            "korune. Prostredníctvom tabuľky sú užívateľovi prezentované zozbierané dáta," \
                            " v pravom dolnom rohu je zobrazený dátum, ktorý zachytáva dátum " \
                            "poslednej aktualizácie zdrojových dát, ktoré sú zachytené v tabuľke."

        self.help_ui2_str = "Dotaz zo skupiny A\n\n" \
                            "Vytvorte rebríček mien, ktoré v danom období najviac posilnili/oslabili"

        self.help_ui3_str = "Dotaz zo skupiny B\n\n" \
                            "Nájdite skupiny mien s podobným chovaním (skupiny mien, ktoré súčasne posilňujú/oslabujú)"

        self.help_ui4_str = "Vlastný dotaz\n\n" \
                            "Zhodnoťte historický vývoj meny vzhľadom ku skupinám s podobným chovaním"

        self.setWindowTitle('UPA Projekt - Analýza devízového trhu')
        self.setStyleSheet('''
            background-color:#adacaa;
            font-family: "Arial"; 
            ''')
        self.Width = 1350
        self.height = 800
        self.resize(self.Width, self.height)
        self.w = None  # No external window yet.

        self.left_widget = QWidget()
        self.left_layout = QVBoxLayout()
        self.main_widget = QWidget()
        self.main_layout = QHBoxLayout()
        self.sp_it_butt = QtWidgets.QSpacerItem(20, 800, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.sp_it_vert = QtWidgets.QSpacerItem(20, 800, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        # add all widgets
        self.btn_1 = QPushButton('Aktuálny prehľad', self)
        self.btn_2 = QPushButton('Dotaz A', self)
        self.btn_3 = QPushButton('Dotaz B', self)
        self.btn_4 = QPushButton('Vlastný dotaz', self)
        self.btn_5 = QPushButton('O projekte', self)
        self.btn_6 = QPushButton('Aktualizovať informácie', self)
        self.btn_7 = QPushButton('Znovu načítať databázu', self)

        self.btn_7.clicked.connect(self.button7)
        self.btn_6.clicked.connect(self.button6)
        self.btn_1.clicked.connect(self.button1)
        self.btn_2.clicked.connect(self.button2)
        self.btn_3.clicked.connect(self.button3)
        self.btn_4.clicked.connect(self.button4)
        self.btn_5.clicked.connect(self.button5)

        # add tabs
        self.tab1 = self.ui1()
        self.tab2 = self.ui2
        self.tab3 = self.ui3()
        self.tab4 = self.ui4()
        self.label_img = QLabel(self)
        self.pixmap = QPixmap('img/FIT_zkracene_barevne_RGB_CZ.resized.png')
        self.right_widget = QTabWidget()

        self.init_ui()

    def reset_database(self):
        self.db.drop_all_tables()
        self.db.create_tables()
        self.db.init_tables(self.db.get_all_nosql_data())

    def get_nosql_data(self, start: str, stop: str):
        ret = {}
        data = self.no_sql_db.select_daily_rates_by_date(start, stop)
        date = 0
        for x in data:
            if not date == x[1]:
                date = x[1]
                ret[f"{x[1]}"] = []
            ret[f"{x[1]}"].append((x[2], x[5], x[4], x[3], x[6]))
        return ret

    def init_ui(self):
        self.label_img.setPixmap(self.pixmap)
        self.left_layout.addWidget(self.label_img)
        self.left_layout.addItem(self.sp_it_vert)
        self.left_layout.addWidget(self.btn_1)
        self.left_layout.addWidget(self.btn_2)
        self.left_layout.addWidget(self.btn_3)
        self.left_layout.addWidget(self.btn_4)
        self.left_layout.addItem(self.sp_it_butt)
        self.left_layout.addWidget(self.btn_5)

        self.left_layout.addStretch(5)
        self.left_layout.setSpacing(50)
        self.left_widget.setObjectName("left_widget")
        self.left_widget.setLayout(self.left_layout)

        self.right_widget.addTab(self.tab1, '')
        self.right_widget.addTab(self.tab2, '')
        self.right_widget.addTab(self.tab3, '')
        self.right_widget.addTab(self.tab4, '')

        self.right_widget.setCurrentIndex(0)
        self.right_widget.setStyleSheet('''
            QTabWidget::pane { border: 0; border-left:2px solid; border-left-color: #909090} 
            QTabBar::tab{width: 0; height: 0; margin: 0; padding: 0; }
            ''')

        self.main_layout.addWidget(self.left_widget)
        self.main_layout.addWidget(self.right_widget)
        self.main_layout.setStretch(0, 40)
        self.main_layout.setStretch(1, 200)
        self.main_widget.setLayout(self.main_layout)
        self.setCentralWidget(self.main_widget)

    # ----------------- 
    # buttons

    def button1(self):
        self.right_widget.setCurrentIndex(0)

    def button2(self):
        self.right_widget.setCurrentIndex(1)

    def button3(self):
        self.right_widget.setCurrentIndex(2)

    def button4(self):
        self.right_widget.setCurrentIndex(3)

    def button5(self):
        self.show_new_window()

    def button6(self):
        self.db.get_update_data_from_nosql()
        query_a_res = self.db.get_actual_data()
        self.table.clearContents()
        self.table.setRowCount(0)
        i = 0
        self.actual_date = query_a_res.pop(0)

        for row in query_a_res:
            j = 0
            self.table.insertRow(i)  # insert new row

            for cell in row:
                new_item = QTableWidgetItem(cell)
                self.table.setItem(i, j, new_item)
                j += 1
            i += 1
        self.table.resizeRowsToContents()
        self.ui_label_act.setText("Dáta su zo dňa %s " % str(self.actual_date))

    def button7(self):
        self.btn_show_dendogram.hide()
        self.reset_database()
        self.button6()

    # ----------------- 
    # pages

    def ui1(self):
        ui_label1 = QLabel('Aktuálny prehľad kurzov mien')
        ui_label1.setStyleSheet('''
            color:white;
            font-size:45px;
            margin-bottom:25px;
            font-style: italic;
            border-bottom: 3px solid red;
        ''')

        self.ui_label_act = QLabel()
        self.ui_label_act.setStyleSheet('''color:grey; font-size:15px;''')
        self.ui_label_act.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)

        main_layout = QVBoxLayout()
        footer_layout = QHBoxLayout()

        self.actual_data = self.db.get_actual_data()
        self.actual_date = self.actual_data.pop(0)

        self.ui_label_act.setText("Dáta su zo dňa %s " % str(self.actual_date))

        self.table = TableViewActual(self.actual_data, len(self.actual_data), 5)
        self.table.verticalHeader().setVisible(False)

        main_layout.addWidget(ui_label1)
        self.table.horizontalHeader().setStyleSheet("font-size:25px;border-radius:14px;color:white")
        self.table.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.table.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        self.table.horizontalHeader().setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        self.table.horizontalHeader().setSectionResizeMode(3, QtWidgets.QHeaderView.Stretch)
        self.table.horizontalHeader().setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)
        self.table.setItemDelegate(AlignDelegate())

        button_box = QDialogButtonBox()
        button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Help)
        button_box.setCenterButtons(False)
        button_box.helpRequested.connect(lambda: self.help_ui(self.help_ui1_str))

        main_layout.addWidget(self.table)
        main_layout.addLayout(footer_layout)

        footer_layout.addWidget(button_box)
        footer_layout.addWidget(self.btn_6)
        footer_layout.addWidget(self.ui_label_act)

        main = QWidget()
        main.setObjectName('mainWindow')
        main.setStyleSheet('''#mainWindow{border:none}''')
        main.setLayout(main_layout)
        return main

    @property
    def ui2(self):
        ui_label1 = QLabel('Rebríček mien')
        ui_label1.setStyleSheet('''
                    color:white;
                    font-size:45px;
                    margin-bottom:25px;
                    font-style: italic;
                    border-bottom: 3px solid red;
                ''')

        main_layout = QVBoxLayout()
        footer_layout = QHBoxLayout()
        date_button_box = QHBoxLayout()

        main_layout.addWidget(ui_label1)

        self.table_query_a.verticalHeader().setVisible(False)
        self.table_query_a.horizontalHeader().setStyleSheet("font-size:25px;border-radius:14px;color:white")
        self.table_query_a.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.table_query_a.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        self.table_query_a.horizontalHeader().setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        self.table_query_a.horizontalHeader().setSectionResizeMode(3, QtWidgets.QHeaderView.Stretch)
        self.table_query_a.horizontalHeader().setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)
        self.table_query_a.horizontalHeader().setSectionResizeMode(5, QtWidgets.QHeaderView.Stretch)

        main_layout.addWidget(self.table_query_a)

        button_box = QDialogButtonBox()
        button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Help)
        button_box.setCenterButtons(False)
        button_box.helpRequested.connect(lambda: self.help_ui(self.help_ui2_str))

        max_date = QDate.fromString(self.db.get_last_date(), 'yyyy-MM-dd')
        min_date = QDate.fromString(self.db.get_first_date(), 'yyyy-MM-dd')

        self.date_edit_from.setMinimumDate(min_date)
        self.date_edit_from.setMaximumDate(max_date)
        self.date_edit_to.setMinimumDate(min_date)
        self.date_edit_to.setMaximumDate(max_date)

        self.date_edit_from.setDateTime(QtCore.QDateTime.currentDateTime())
        self.date_edit_from.setDate(min_date)
        date_button_box.addWidget(self.date_edit_from)
        self.date_edit_from.dateChanged.connect(self.act_date_from)

        self.date_edit_to.setDateTime(QtCore.QDateTime.currentDateTime())
        self.date_edit_to.setDate(max_date)
        date_button_box.addWidget(self.date_edit_to)
        self.date_edit_to.dateChanged.connect(self.act_date_to)

        btn_show = QPushButton('Zobraziť rebríček', self)
        btn_show.clicked.connect(self.get_query_a_res)

        date_button_box.addWidget(btn_show)

        main_layout.addLayout(date_button_box)

        main_layout.addLayout(footer_layout)
        footer_layout.addWidget(button_box)

        main = QWidget()
        main.setObjectName('mainWindow')
        main.setStyleSheet('''#mainWindow{border:none}''')
        main.setLayout(main_layout)
        return main

    def ui3(self):
        ui_label1 = QLabel('Ovplyvňovanie charakteru vývoja kurzu')
        ui_label1.setStyleSheet('''
                            color:white;
                            font-size:45px;
                            margin-bottom:25px;
                            font-style: italic;
                            border-bottom: 3px solid red;
                        ''')
        main_layout = QVBoxLayout()
        footer_layout = QHBoxLayout()

        date_button_box = QHBoxLayout()

        main_layout.addWidget(ui_label1)

        self.table_query_b.verticalHeader().setVisible(False)
        self.table_query_b.horizontalHeader().setStyleSheet("font-size:25px;border-radius:14px;color:white")
        self.table_query_b.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.table_query_b.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)

        main_layout.addWidget(self.table_query_b)

        button_box = QDialogButtonBox()
        button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Help)
        button_box.setCenterButtons(False)
        button_box.helpRequested.connect(lambda: self.help_ui(self.help_ui3_str))

        max_date = QDate.fromString(self.db.get_last_date(), 'yyyy-MM-dd')
        min_date = QDate.fromString(self.db.get_first_date(), 'yyyy-MM-dd')

        self.date_edit_from_b.setMinimumDate(min_date)
        self.date_edit_from_b.setMaximumDate(max_date)
        self.date_edit_to_b.setMinimumDate(min_date)
        self.date_edit_to_b.setMaximumDate(max_date)

        self.date_edit_from_b.setDateTime(QtCore.QDateTime.currentDateTime())
        self.date_edit_from_b.setDate(min_date)
        date_button_box.addWidget(self.date_edit_from_b)
        self.date_edit_from_b.dateChanged.connect(self.act_date_from_b)

        self.date_edit_to_b.setDateTime(QtCore.QDateTime.currentDateTime())
        self.date_edit_to_b.setDate(max_date)
        date_button_box.addWidget(self.date_edit_to_b)
        self.date_edit_to_b.dateChanged.connect(self.act_date_to_b)

        btn_show = QPushButton('Zobraziť rebríček', self)
        btn_show.clicked.connect(self.get_query_b_res)
        self.btn_show_dendogram = QPushButton('Zobraziť hierarchický graf', self)
        self.btn_show_dendogram.clicked.connect(self.show_dendogram_window)


        date_button_box.addWidget(btn_show)

        main_layout.addLayout(date_button_box)

        main_layout.addLayout(footer_layout)
        footer_layout.addWidget(button_box)
        footer_layout.addWidget(self.btn_show_dendogram)
        self.btn_show_dendogram.hide()

        main = QWidget()
        main.setObjectName('mainWindow')
        main.setStyleSheet('''#mainWindow{border:none}''')
        main.setLayout(main_layout)
        return main

    def ui4(self):
        ui_label1 = QLabel('Vlastný dotaz')
        ui_label1.setStyleSheet('''
                                    color:white;
                                    font-size:45px;
                                    margin-bottom:25px;
                                    font-style: italic;
                                    border-bottom: 3px solid red;
                                ''')
        main_layout = QVBoxLayout()
        footer_layout = QHBoxLayout()

        date_button_box = QHBoxLayout()

        main_layout.addWidget(ui_label1)

        button_box = QDialogButtonBox()
        button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Help)
        button_box.setCenterButtons(False)
        button_box.helpRequested.connect(lambda: self.help_ui(self.help_ui4_str))

        max_date = QDate.fromString(self.db.get_last_date(), 'yyyy-MM-dd')
        min_date = QDate.fromString(self.db.get_first_date(), 'yyyy-MM-dd')

        self.date_edit_from_custom.setMinimumDate(min_date)
        self.date_edit_from_custom.setMaximumDate(max_date)
        self.date_edit_to_custom.setMinimumDate(min_date)
        self.date_edit_to_custom.setMaximumDate(max_date)

        self.date_edit_from_custom.setDateTime(QtCore.QDateTime.currentDateTime())
        self.date_edit_from_custom.setDate(min_date)
        date_button_box.addWidget(self.date_edit_from_custom)
        self.date_edit_from_custom.dateChanged.connect(self.act_date_from_custom)

        self.date_edit_to_custom.setDateTime(QtCore.QDateTime.currentDateTime())
        self.date_edit_to_custom.setDate(max_date)
        date_button_box.addWidget(self.date_edit_to_custom)
        self.date_edit_to_custom.dateChanged.connect(self.act_date_to_custom)

        self.categories = []
        self.set0 = QBarSet("")
        self.series = QBarSeries()

        self.chart = QChart()
        self.chart.setAnimationOptions(QChart.SeriesAnimations)

        self.axis = QBarCategoryAxis()
        self.axis.append(self.categories)
        self.chart.createDefaultAxes()
        self.axis.setLabelsAngle(-90)

        self.chart.legend().setVisible(False)

        self.series = QBarSeries()
        self.series.append(self.set0)
        self.chart.addSeries(self.series)
        self.chart.setAxisX(self.axis, self.series)

        self.axisY = QValueAxis()
        self.axisY.setRange(0,0)
        self.chart.setAxisY(self.axisY)

        chartView = QChartView(self.chart)
        chartView.setRenderHint(QPainter.Antialiasing)

        main_layout.addWidget(chartView)


        btn_show = QPushButton('Zhodnotiť volatilitu mien', self)
        btn_show.clicked.connect(self.get_query_custom_res)

        date_button_box.addWidget(btn_show)

        main_layout.addLayout(date_button_box)

        main_layout.addLayout(footer_layout)
        footer_layout.addWidget(button_box)
        footer_layout.addWidget(self.btn_7)

        main = QWidget()
        main.setObjectName('mainWindow')
        main.setStyleSheet('''#mainWindow{border:none}''')
        main.setLayout(main_layout)
        return main

    # -----------------
    # functions

    def show_dendogram_window(self):
        self.w = DendogramWindow()
        self.w.setWindowTitle("Hierachia tried")
        self.w.show()

    def percentage(self, diff, whole):
        return 100 * float(diff) / float(whole)

    def show_new_window(self):
        self.w = AnotherWindow()
        self.w.show()

    def help_ui(self, help_str):
        self.w = HelpWindow(help_str)
        self.w.setWindowTitle("Pomoc")
        self.w.show()

    def act_date_from(self, new_date_from):
        self.date_edit_from.setDate(new_date_from)

    def act_date_to(self, new_date_to):
        self.date_edit_to.setDate(new_date_to)

    def act_date_from_b(self, new_date_from):
        self.date_edit_from_b.setDate(new_date_from)

    def act_date_to_b(self, new_date_to):
        self.date_edit_to_b.setDate(new_date_to)

    def act_date_from_custom(self, new_date_from):
        self.date_edit_from_custom.setDate(new_date_from)

    def act_date_to_custom(self, new_date_to):
        self.date_edit_to_custom.setDate(new_date_to)

    def get_query_a_res(self, ):
        if self.date_edit_from.date() > self.date_edit_to.date():
            self.w = HelpWindow("Zadaný zlý rozsah dátumov")
            self.w.setWindowTitle("Chyba")
            self.w.show()
        else:
            res = self.db.get_data_by_date(
                self.date_edit_from.date().toString('yyyy-MM-dd'),
                self.date_edit_to.date().toString('yyyy-MM-dd')
            )
            if not res[0]:
                self.help_ui('Pre dátum %s sa v databáze nenachádz žiaden záznam'
                             % self.date_edit_from.date().toString('dd. MM. yyyy'))
            if not res[1]:
                self.help_ui('Pre dátum %s sa v databáze nenachádza žiaden záznam'
                             % self.date_edit_to.date().toString('dd. MM. yyyy'))

            self.table_query_a.clearContents()
            self.table_query_a.setRowCount(0)

            query_a_res = self.calculate_query_a(res)
            i = 0

            for row in query_a_res:
                self.table_query_a.insertRow(i)  # insert new row

                item = QTableWidgetItem(str(row))
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.table_query_a.setItem(i, 0, item)
                item = QTableWidgetItem(str(query_a_res[row][0]))
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.table_query_a.setItem(i, 1, item)
                item = QTableWidgetItem(str(query_a_res[row][2]))
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.table_query_a.setItem(i, 2, item)
                item = QTableWidgetItem(str(query_a_res[row][3]))
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.table_query_a.setItem(i, 3, item)
                item = QTableWidgetItem(str("%.2f" % query_a_res[row][4]))
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.table_query_a.setItem(i, 4, item)
                item = QTableWidgetItem(str("%.2f %%" % query_a_res[row][5]))
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.table_query_a.setItem(i, 5, item)

                i += 1
            self.table_query_a.resizeRowsToContents()

    def get_query_b_res(self, ):
        self.btn_show_dendogram.show()

        if self.date_edit_from_b.date() > self.date_edit_to_b.date():
            self.w = HelpWindow("Zadaný zlý rozsah dátumov")
            self.w.setWindowTitle("Chyba")
            self.w.show()
        else:
            res = self.db.get_data_from_to_date(
                self.date_edit_from_b.date().toString('yyyy-MM-dd'),
                self.date_edit_to_b.date().toString('yyyy-MM-dd')
            )
            if not res:
                self.help_ui('V databáze nenachádza žiaden záznam pre daný rozsah'
                             % self.date_edit_from_b.date().toString('dd. MM. yyyy'))

            self.table_query_b.clearContents()
            self.table_query_b.setRowCount(0)

            query_b_res = self.calculate_query_b(res)
            # clust_count = len(set([x[1] for x in query_b_res]))

            # sort answer
            freq_map = defaultdict(int)
            for idx, val in query_b_res:
                freq_map[val] += 1
            query_b_res = sorted(query_b_res, key=lambda ele: freq_map[ele[1]], reverse=True)

            temp_result = dict()
            set_of_cluster = set()
            for record in query_b_res:
                set_of_cluster.add(record[1])
            for classes in set_of_cluster:
                temp_result[classes] = []
            for classes in query_b_res:
                temp_result[classes[1]].append(classes[0])

            i = 0
            for row in temp_result:
                string_to_table = ''
                self.table_query_b.insertRow(i)  # insert new row

                item = QTableWidgetItem(str(row))
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.table_query_b.setItem(i, 0, item)
                for code in temp_result[row]:
                    string_to_table += ('%s\n' % code)
                string_to_table = string_to_table[:-2]
                item = QTableWidgetItem(string_to_table)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.table_query_b.setItem(i, 1, item)
                i += 1
            self.table_query_b.resizeRowsToContents()

    def get_query_custom_res(self, ):
        if self.date_edit_from_custom.date() > self.date_edit_to_custom.date():
            self.w = HelpWindow("Zadaný zlý rozsah dátumov")
            self.w.setWindowTitle("Chyba")
            self.w.show()
        else:
            res = self.db.get_data_from_to_date(
                self.date_edit_from_custom.date().toString('yyyy-MM-dd'),
                self.date_edit_to_custom.date().toString('yyyy-MM-dd')
            )
            if not res:
                self.help_ui('V databáze nenachádza žiaden záznam pre daný rozsah'
                             % self.date_edit_from_custom.date().toString('dd. MM. yyyy'))


            query_a_res = self.calculate_query_custom(res)
            self.set0 = QBarSet("")
            self.categories = []
            self.chart.removeAllSeries()
            self.chart.removeAxis(self.axis)
            self.chart.removeAxis(self.axisY)
            max = 0
            for key in query_a_res:
                if max < query_a_res[key]: max = query_a_res[key]
                self.set0.append(query_a_res[key])
                self.categories.append(key)

            self.axis = QBarCategoryAxis()
            self.axis.append(self.categories)
            self.chart.createDefaultAxes()
            self.axis.setLabelsAngle(-90)

            self.axisY.setRange(0, max)
            self.chart.setAxisY(self.axisY)
            self.series = QBarSeries()
            self.series.append(self.set0)
            self.chart.addSeries(self.series)
            self.chart.setAxisX(self.axis, self.series)



    def calculate_query_a(self, data_query_a):
        temp_result = {}
        result = {}
        for record in data_query_a[0]:
            temp_result[record[2]] = (record[0], record[1], record[3])
        for record in data_query_a[1]:
            result[record[2]] = temp_result.get(record[2]) + \
                                (record[3],
                                 record[3] - temp_result.get(record[2])[2],
                                 self.percentage(record[3] - temp_result.get(record[2])[2],
                                                 temp_result.get(record[2])[2]))

        x = sorted(result, key=lambda k: result[k][5], reverse=True)
        temp_result = {}
        for item in x:
            temp_result[item] = result.get(item)
        return temp_result

    def change_perc(self, older: Tuple, newer: Tuple) -> Tuple[float, int]:
        return ((older[1] - newer[1]) / older[1] * (newer[2].day - older[2].day),
                newer[2].day - older[2].day)


    def prepare_data_change(self, data):
        ret = {}
        curr = ''
        ret[f'{data[0][0]}'] = []
        # create matrix of continuous change
        for idx, row in enumerate(data[1:], start=1):
            old_row = data[idx - 1]
            # same currency
            if old_row[0] == row[0]:
                change, rep = self.change_perc(old_row, row)
                for x in range(rep):
                    ret[f'{row[0]}'].append(change)
            else:
                ret[f'{row[0]}'] = []

        # make sure that data is same length
        max = 0
        for key in ret.keys():
            ln = len(ret[key])
            if ln > max:
                max = ln

        for key in ret.keys():
            ln = len(ret[key])
            if not ln < max:
                ret[key].extend([0 for i in range(max - ln)])

        return ret

    def calculate_query_b(self, data_query_b, cl_nmbr=4):  # -> List[Tuple[str, int], ...]
        data = self.prepare_data_change(data_query_b)
        change_series = pd.DataFrame()

        change_series = pd.DataFrame.from_dict(data, orient='index')

        # do the clustering
        Z = hac.linkage(change_series, method='ward', metric='euclidean')

        self.plot_dendogram(Z, list(data.keys()))

        results = fcluster(Z, cl_nmbr, criterion='maxclust')
        # results = fcluster(Z, 0.25, criterion='distance')

        return [(key, results[idx]) for idx, key in enumerate(data.keys())]

    def plot_dendogram(self, Z, labels: List = None):
        # Plot dendogram
        plt.figure(figsize=(12, 6))
        plt.title('Dendrogram Hierarchickej Klasterizácie', fontsize=22)
        #plt.xlabel('Skratky mien', fontsize=18)
        plt.ylabel('Euklidovská vzdialenosť skupín', fontsize=18)
        hac.dendrogram(
            Z,
            labels=labels,
            leaf_rotation=90.,  # rotates the x axis labels
            leaf_font_size=15.,  # font size for the x axis labels
        )
        plt.savefig('./img/dendogram.png')

    def change_log(self, older: Tuple, newer: Tuple) -> Tuple[float, int]:
        return (log(newer[1] / older[1]), newer[2].day - older[2].day)

    def prepare_data_log_change(self, data):
        ret = {}
        curr = ''
        ret[f'{data[0][0]}'] = []
        # create matrix of continuous change
        for idx, row in enumerate(data[1:], start=1):
            old_row = data[idx - 1]
            # same currency
            if old_row[0] == row[0]:
                change, rep = self.change_log(old_row, row)
                for x in range(rep):
                    ret[f'{row[0]}'].append(change)
            else:
                ret[f'{row[0]}'] = []

        # make sure that data is same length
        max = 0
        for key in ret.keys():
            ln = len(ret[key])
            if ln > max:
                max = ln

        for key in ret.keys():
            ln = len(ret[key])
            if not ln < max:
                ret[key].extend([0 for i in range(max - ln)])

        return ret

    def calculate_query_custom(self, data_query_custom):
        data = self.prepare_data_log_change(data_query_custom)
        res = {}
        for key in data.keys():
            r_mean = mean(data[key])
            diff_square = [(data[key][i] - r_mean) ** 2 for i in range(0, len(data[key]))]
            std = sqrt(sum(diff_square) * (1.0 / (len(data[key]) - 1)))
            res[key] = (std * sqrt(252)) * 100
        return res


    def close(self):
        if os.path.exists("img/dendogram.png"):
            os.remove("img/dendogram.png")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("img/icon.png"), QtGui.QIcon.Selected, QtGui.QIcon.On)
    app.setWindowIcon(icon)
    ex = Window()
    ex.show()
    sys.exit(app.exec_())
