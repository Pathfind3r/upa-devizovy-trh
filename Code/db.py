import re
from datetime import datetime
from urllib.request import urlopen
from datetime import datetime as dt

from cassandra.cluster import Cluster
from cassandra.cluster import ResultSet
from cassandra.query import SimpleStatement
from typing import Dict, List, Tuple

from country_list import Country_list
#from web_scrape.main import scrape_data


class Cass_db:
    tables: Dict[str, Dict[str, bool]] = {
        'daily_rates': {
            'year': False,
            'date': True,
            'currency_code': True,
            'coefficient': True,
            'country_name': True,
            'currency_name': True,
            'value': False

        },
        'countries': {
            'partition': False,
            'currency_code': True,
            'coefficient': True,
            'country_name': True,
            'currency_name': True,
        }
    }

    def __init__(self, ip: str = '127.0.0.1', port: int = 9042) -> None:
        self.cluster = Cluster(contact_points=[ip], port=port)
        self.session = self.cluster.connect()

    def __del__(self) -> None:
        self.cluster.shutdown()

    @staticmethod
    def convert_date_format(date: str) -> str:
        return "-".join(list(reversed(date.split('.'))))

    @staticmethod
    def date_to_year(date: str) -> str:
        """
        Returns year form date.
        :param date: date string in format YYYY-MM-DD
        """
        return date.split('-', 1)[0]

    def prepare(self, key_space: str, strategy: str = 'SimpleStrategy', repl_factor: int = 3) -> None:
        """
        Prepares Cassandra keyspace and selects it for use.
        :param key_space: name of Cassandras main keyspace
        :param strategy: Replica Placement strategy, default SimpleStrategy
        :param repl_factor: number of replicas of data, default 3
        :return: None
        """
        query = f""" 
            CREATE KEYSPACE IF NOT EXISTS {key_space}
            WITH replication = {{
                'class':'{strategy}', 
                'replication_factor' : {repl_factor}
                }};
            """
        self.session.execute(query)
        self.session.execute(f"USE {key_space};")
        self.create_daily_rates_table()
        self.create_country_table()

    def recreate(self, key_space: str, strategy: str = 'SimpleStrategy', repl_factor: int = 3) -> None:
        """
        Drops cassandra keyspace and creates it form scratch (by calling prepare(..) method).
        :param key_space: name of Cassandras main keyspace
        :param strategy: Replica Placement strategy, default SimpleStrategy
        :param repl_factor: number of replicas of data, default 3
        :return: None
        """
        self.session.execute(f"DROP KEYSPACE IF EXISTS {key_space}")
        self.prepare(key_space, strategy, repl_factor)

    def create_daily_rates_table(self) -> None:
        query = fr"""
            CREATE TABLE IF NOT EXISTS daily_rates(
            year smallint,
            date date,
            currency_code text,
            coefficient text,
            country_name text,
            currency_name text,  
            value decimal,
            PRIMARY KEY ( year, date, currency_code)      
            );
        """
        # self.session.execute("DROP TABLE IF EXISTS daily_rates")
        self.session.execute(query)

    def create_country_table(self) -> None:
        query = fr"""
            CREATE TABLE IF NOT EXISTS countries(
            partition smallint,
            currency_code text,
            coefficient text,
            country_name text,
            currency_name text,    
            PRIMARY KEY (partition, currency_code)
            );
        """
        # self.session.execute("DROP TABLE IF EXISTS countries")
        self.session.execute(query)

    def store(self, table: str, data: List[Tuple[str, ...]]) -> None:
        """
        Stores data to table
        :param table: Name of table to store data
        :param data: Data to store to table. First row defines column_list subsequent rows define data.
        """
        data_iter = iter(data)
        next(data_iter)
        for row in data_iter:
            query = fr"""
                INSERT INTO {table} (
                    {", ".join(data[0])}
                ) VALUES (
                    {", ".join(
                [fr"'{item}'" if self.tables[table][data[0][idx]] else item
                 for idx, item in enumerate(row)]
            )}
                );
            """
            self.session.execute(query)

    def store_countries_table(self, data: List[Tuple[str, str, str, str]]) -> None:
        ret: List[Tuple[str, str, str, str, str]] = []
        for x in data:
            ret.append(tuple('1') + x)
        self.store("countries", [("partition", 'currency_code', 'coefficient',
                                  'country_name', 'currency_name',)] + ret)

    def store_daily_rates_table(self, data: List[Tuple[str, str, str, str, str, str, str]]) -> None:
        self.store("daily_rates", [("year", 'date', 'currency_code', 'coefficient', 'country_name',
                                    'currency_name', 'value')] + data)

    def select_daily_rates_by_date(self, start: str = None, stop: str = None) -> List[List]:
        """
        :param start: format YYYY-MM-DD
        :param stop: format YYYY-MM-DD
        :return: 2D array of data from specific timeframe
        """
        res: List[List[...]] = []
        for year in range(int(self.date_to_year(start)), int(self.date_to_year(stop)) + 1):
            query = fr"""
                SELECT * FROM daily_rates 
                WHERE year = {year} 
                    AND date >= '{start}' AND date <= '{stop}'
            """
            ret = self.session.execute(query)
            res.extend(ret.all())
        return res

    def select_all_countries(self) -> List[Tuple[str, str, str, str]]:
        query = f"""
            SELECT currency_code, country_name, currency_name, coefficient FROM countries
            WHERE partition = 1
        """
        return self.session.execute(query).all()

    def should_init(self) -> bool:
        return len(self.session.execute("SELECT year FROM daily_rates LIMIT 1;").all()) == 0

    def init_nosql_data(self):
        # scrape all data
        country_list = Country_list(self)
        now = datetime.now()
        for year in range(1991, now.year + 1):
            self.store_daily_rates_table(self.scrape_data(year, country_list))
            country_list.store()

    def update_nosql_data(self):
        now = datetime.now()
        last_date = []
        for year in range(now.year, 1991, -1):
            query = f"""
            SELECT * FROM daily_rates WHERE year = {year} ORDER BY date DESC limit 1;
            """
            resp = self.session.execute(query).all()
            if len(resp) > 0:
                last_date = str(resp[0][1])
                break

        if len(last_date) == 0:
            self.init_nosql_data()
        else:
            country_list = Country_list(self)
            now = datetime.now()
            for year in range(int(last_date.split('-')[0]), now.year + 1):
                self.store_daily_rates_table(self.scrape_data(year, country_list, last_date))
                country_list.store()

    def scrape_data(self, year: int, country_list, date_threshold_str: str = '') -> List[Tuple[str, str, str, str, str, str, str]]:
        date_threshold = ''
        if len(date_threshold_str):
            date_threshold = dt.strptime(date_threshold_str, "%Y-%m-%d")
        ret = []
        url = f"https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/rok.txt?rok={year}"
        page = urlopen(url)
        html_bytes = page.read()
        html = html_bytes.decode("utf-8")
        head = []
        # table = list(filter(None, re.split(r'(Datum.*\n)', html)))
        # for idx_tab, section in enumerate(table):
        scrape = False
        for section in filter(None, re.split(r'(Datum.*\n)', html)):
            if not section[0].isnumeric():
                scrape = True
                # head = scrape_country_name(table[idx_tab + 1].split('|', 1)[0])
                for val in filter(None, re.split(r'[|\n]', section)[1:]):
                    head.append(val.split(" "))
            else:
                for row in filter(None, section.split('\n')):
                    date_str, row = row.split('|', 1)
                    # scrape for country names
                    if scrape:
                        for c in head:
                            country_list.scrape(date_str, c[1])
                        scrape = False
                    date_str = Cass_db.convert_date_format(date_str)
                    # check to load only dates after threshold (when used form update_nosql_data)
                    if len(date_threshold_str):
                        date = dt.strptime(date_str, "%Y-%m-%d")
                        if date <= date_threshold:
                            # date is below threshold so skip this date
                            continue
                    for idx, val in enumerate(row.split('|')):
                        country = country_list.get(head[idx][1], head[idx][0])
                        ret.append((str(Cass_db.date_to_year(date_str)),
                                    str(date_str),
                                    str(country[0]),
                                    str(country[1]),
                                    str(country[2]),
                                    str(country[3]),
                                    str(val.replace(',', '.'))))
        return ret

    # def load(self, table: str) -> None:
